
class Employee {
    constructor(name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    
    set name(value) {
        this._name = value;
    }
    
    get name() {
        return this._name;
    }
    
    set age(value) {
        this._age = value;
    }
    
    get age() {
        return this._age;
    }
    
    set salary(value) {
        this._salary = value;
    }
    
    get salary() {
        return this._salary;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang){
        super(name, age, salary);
        this.lang = lang;
    } 

    get salary() {
        return this._salary*3;
    }
}

let programmer = new Programmer ("Djon", "39", "2700", "C++");
let backEnd = new Programmer ("Bonny", "25", "2500", "Pyton");
let frontEnd = new Programmer ("Julia", "29", "2000", "Java");

console.log(programmer);
console.log(backEnd);
console.log(frontEnd);
